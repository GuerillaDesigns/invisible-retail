<?php
function gd_write_schema(){
	$schema = array();
	$graph = array();
	$graphBase = array();
	$graphBlog = array();
	$graphEvents = array();

// schema data
	$graphBase["@type"] = "LocalBusiness";
    $graphBlog["@type"] = "Blog";

//Name
	$sld_name = get_field("ic_ss_bi_bn", "option");
	if (!$sld_name) {
		$sld_name = get_bloginfo('name');
	}
	$graphBase["name"] = $sld_name;
    $graphBlog["name"] = $sld_name;

//URL
	$blogUrl = get_bloginfo('url');
	$graphBase["url"] = $blogUrl;
    $graphBlog["url"] = $blogUrl;
    
// logo
	if(get_field("ic_ss_bs_pl", "option")){
		$sld_logo = get_field("ic_ss_bs_pl", "option");
	}
	if ($sld_logo) {
		$graphBase["logo"] = $sld_logo['url'];
	}
	$sld_image = $sld_logo['url'];
	if ($sld_image) {
		$graphBase["image"] = $sld_image;
        $graphBlog["image"] = $sld_image;
    }
    
// Description
	$sld_description = get_bloginfo('description');
	if(get_field("ic_ss_bi_bd", "option")){
		$sld_description = get_field("ic_ss_bi_bd", "option");
	}
	if ($sld_description) {
		$graphBase["description"] = $sld_description;
        $graphBlog["description"] = $sld_description;
    }
    
// Telephone
	$sld_main_telephone = get_field('ic_ss_bi_bmp', 'options');
	$sld_main_telephone = preg_replace('/[^0-9]/', '', $sld_main_telephone);
	if ($sld_main_telephone) {
		$graphBase["telephone"] = '+1' . $sld_main_telephone;
    }
    
// Hours
    $openHours = "";
    // MON
    $mon = 'Mo';
    if(get_field('ic_ss_bi_bh_mon', 'options')) {
        $monday = get_field('ic_ss_bi_bh_mon', 'options');
        $monHours = '';
        foreach($monday['hd_hours'] as $shift) {
            $shiftOpen = $shift['hd_opening_time'];
            $shiftClose = $shift['hd_closing_time'];
            $hours = $shiftOpen . '-' . $shiftClose;
            $monHours .= ' '.$hours;
        }
        $mon = '"Mo'.$monHours.'"';
    }
    // TUES
    $tues = 'Tu';
    if(get_field('ic_ss_bi_bh_tues', 'options')) {
        $tuesday = get_field('ic_ss_bi_bh_tues', 'options');
        $tuesHours = '';
        foreach($tuesday['hd_hours'] as $shift) {
            $shiftOpen = $shift['hd_opening_time'];
            $shiftClose = $shift['hd_closing_time'];
            $hours = $shiftOpen . '-' . $shiftClose;
            $tuesHours .= ' '.$hours;
        }
        $tues = '"Tu'.$tuesHours.'"';
    }
    // WED
    $wed = 'We';
    if(get_field('ic_ss_bi_bh_wed', 'options')) {
        $wednesday = get_field('ic_ss_bi_bh_wed', 'options');
        $wedHours = '';
        foreach($wednesday['hd_hours'] as $shift) {
            $shiftOpen = $shift['hd_opening_time'];
            $shiftClose = $shift['hd_closing_time'];
            $hours = $shiftOpen . '-' . $shiftClose;
            $wedHours .= ' '.$hours;
        }
        $wed = '"We'.$wedHours.'"';
    }
    // THUR
    $thur = 'Th';
    if(get_field('ic_ss_bi_bh_thur', 'options')) {
        $thursday = get_field('ic_ss_bi_bh_thur', 'options');
        $thurHours = '';
        foreach($thursday['hd_hours'] as $shift) {
            $shiftOpen = $shift['hd_opening_time'];
            $shiftClose = $shift['hd_closing_time'];
            $hours = $shiftOpen . '-' . $shiftClose;
            $thurHours .= ' '.$hours;
        }
        $thur = '"Th'.$thurHours.'"';
    }
    // FRI
    $fri = 'Fr';
    if(get_field('ic_ss_bi_bh_fri', 'options')) {
        $friday = get_field('ic_ss_bi_bh_fri', 'options');
        $friHours = '';
        foreach($friday['hd_hours'] as $shift) {
            $shiftOpen = $shift['hd_opening_time'];
            $shiftClose = $shift['hd_closing_time'];
            $hours = $shiftOpen . '-' . $shiftClose;
            $friHours .= ' '.$hours;
        }
        $fri = '"Fr'.$friHours.'"';
    }
    // SAT
    $sat = 'Sa';
    if(get_field('ic_ss_bi_bh_sat', 'options')) {
        $saturday = get_field('ic_ss_bi_bh_sat', 'options');
        $satHours = '';
        foreach($saturday['hd_hours'] as $shift) {
            $shiftOpen = $shift['hd_opening_time'];
            $shiftClose = $shift['hd_closing_time'];
            $hours = $shiftOpen . '-' . $shiftClose;
            $satHours .= ' '.$hours;
        }
        $sat = '"Sa'.$satHours.'"';
    }
    // SUN
    $sun = 'Su';
    if(get_field('ic_ss_bi_bh_sun', 'options')) {
        $sunday = get_field('ic_ss_bi_bh_sun', 'options');
        $sunHours = '';
        foreach($sunday['hd_hours'] as $shift) {
            $shiftOpen = $shift['hd_opening_time'];
            $shiftClose = $shift['hd_closing_time'];
            $hours = $shiftOpen . '-' . $shiftClose;
            $sunHours .= ' '.$hours;
        }
        $sun = '"Su'.$sunHours.'"';
    }
    $openHours = '['.$mon.', '.$tues.', '.$wed.', '.$thur.', '.$fri.', '.$sat.', '.$sun.']';
    $graphBase["openingHours"] = $openHours;

// Geo
	$sld_geo_coordinates_lat = get_field("ic_ss_bi_gclat", "option");
	$sld_geo_coordinates_long = get_field("ic_ss_bi_gclon", "option");
	if ($sld_geo_coordinates_lat || $sld_geo_coordinates_long) {
		$graphBase["geo"] = array(
			"@type" => "GeoCoordinates",
			"latitude" => $sld_geo_coordinates_lat,
			"longitude" => $sld_geo_coordinates_long
		);
    }
    
// Address
	$sld_address = get_field("ic_ss_bi_ba", "options");
	$sld_city = get_field("ic_ss_bi_bc", "options");
	$sld_state = get_field("ic_ss_bi_bs", "options");
	$sld_zipcode = get_field("ic_ss_bi_bz", "options");
	if ($sld_address || $sld_city || $sld_state || $sld_zipcode) {
		$graphBase["address"] = array("@type" => "PostalAddress");
		if ($sld_address) {
			$graphBase["address"]["streetAddress"] = $sld_address;
		}
		if ($sld_city) {
			$graphBase["address"]["addressLocality"] = $sld_city;
		}
		if ($sld_city) {
			$graphBase["address"]["addressRegion"] = $sld_city;
		}
		if ($sld_zipcode) {
			$graphBase["address"]["postalCode"] = $sld_zipcode;
		}
    }

// Price Range
    $graphBase["priceRange"] = '$$';
    
// Map
	$sld_maplink = get_field('ic_ss_bi_bmap', 'options');
	if ($sld_maplink && ($sld_type !== 'Organization')) {
		$graphBase["hasMap"] = $sld_maplink;
    }

// Other Web sites
	$other_web_properties = get_field('ic_ss_bi_owp', 'options');
	if ($other_web_properties) {
		$graphBase["sameAs"] = array();
		foreach ($other_web_properties as $other_web_property) {
			array_push($graphBase["sameAs"], $other_web_property['ic_ss_bi_owp_url']);
		}
	}

// Contact Points
	$sld_contact_points = get_field('ic_ss_bi_cps', 'options');
	if ($sld_contact_points) {
		$graphBase["contactPoint"] = array();
		foreach ($sld_contact_points as $sld_contact_point) {
			$contact = array();
			$contact["@type"] = "ContactPoint";
			$sld_contact_type = $sld_contact_point['ic_ss_bi_cpt'];
			if ($sld_contact_type) {
				$contact["contactType"] = 'customer service';
			}
			$sld_contact_phone = $sld_contact_point['ic_ss_bi_cpp'];
			if ($sld_contact_phone) {
				$contact["telephone"] = '+1' . $sld_contact_phone;
			}
			$sld_contact_email = $sld_contact_point['ic_ss_bi_cpe'];
			if ($sld_contact_email) {
				$contact["email"] = $sld_contact_email;
			}
			array_push($graphBase["contactPoint"], $contact);
		}
	}
	//todo work out events and figure out what kind of schema type we should be using
// Schema Events
	if (is_plugin_active('the-events-calendar/the-events-calendar.php')) {
		// Plugin is active
		// Retrieve the next 5 upcoming events
		$today_date = new dateTime();
		$events = tribe_get_events([
			'posts_per_page' => 5,
			'start_date' => get_object_vars($today_date)['date']
		]);
		if ($events != "") :
            $graphEvents["event"] = array();
			foreach ($events as $event) {
				$event_id = $event->ID;
				setup_postdata($event);
				$event = array();
				if (!tribe_get_event_meta($event_id, '_EventHideFromUpcoming')) {
					$eTitle = get_the_title($event_id);
					$eDesc = get_the_excerpt($event_id);
					$eVenueName = tribe_get_venue($event_id);
					$eAddress = tribe_get_address($event_id);
					$eCity = tribe_get_city($event_id);
					$eState = tribe_get_stateprovince($event_id);
					$eZip = tribe_get_zip($event_id);
					$eStart = tribe_get_start_date( $event_id, true, 'm-d-Y h:i');
					$eEnd = tribe_get_end_date( $event_id, true, 'm-d-Y h:i');
					$eUrl = tribe_get_event_link($event_id);
					$event["@type"] = "Event";
					$eventImage = get_the_post_thumbnail_url($event_id);
					$event['image']['@type'] = "imageObject";
					$event['image'] = $sld_image;
                    if($eventImage) {
                        $event["image"] = $eventImage;
                    }
					$event["location"] = array(
						"@type" => "Place",
						"address" => array(
							"streetAddress" => $eAddress,
							"addressLocality" => $eCity,
							"addressRegion" => $eState,
							"postalCode" => $eZip
						),
                        "name" => $eVenueName
					);
                    $event["name"] = $eTitle;
					$event['description'] = $eDesc;
					$event["startDate"] = $eStart;
					$event["endDate"] = $eEnd;
					$event["url"] = $eUrl;
					array_push($graphEvents["event"], $event);
				}
			}
		endif;
	}
// Blog
        $blogArgs = array(
			'numberofposts' => '-1',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_status' => 'publish'
        );// query posts
        $recent_posts = get_posts($blogArgs, ARRAY_A);
        $graphBlog['blogPosts'] = array();
        foreach ($recent_posts as $post) {
            setup_postdata($post);
            $ID = $post->ID;
            $title = $post->post_title;
            //$author = $post->post_author;
            $url = $post->guid;
            if ($post->post_excerpt) {
                $excerpt = $post->post_excerpt;
            } else {
                $excerpt = $post->post_content;
                $excerpt = substr($excerpt, 0, 280);
            };
            $published = $post->post_date;
            $dateModified  = $post->post_modified;
            $image = get_the_post_thumbnail_url($ID);
            $bPost = array();
            $bPost['@type'] = "blogPosting";
            $bPost['mainEntityOfPage'] = $url;
            $bPost['headline'] = $title;
            $bPost['description'] = $excerpt;
            $bPost['author']['@type'] = "Organization";
            $bPost['author']['name'] = $sld_name;
            $bPost["datePublished"] = $published;
            $bPost["dateCreated"] = $published;
			$bPost["dateModified"] = $dateModified;
			$bPost['image'] = array();
			$bPost['image']['@type'] = "imageObject";
			$bPost['image'] = $sld_image;
            if ($image) {
                $bPost['image'] = $image;
                $schema["image"] = $sld_image.'passed/image.png';
			} 
            $bPost['publisher'] = array();
            $bPost['publisher']['@type'] = "Organization";
            $bPost['publisher']['name'] = $sld_name;
            $bPost['publisher']['logo'] = array();
            $bPost['publisher']['logo']['@type'] = "imageObject";
            $bPost['publisher']['logo']['url'] = $sld_logo['url'];
            array_push($graphBlog['blogPosts'], $bPost);
        }
    wp_reset_query();
	// Format the arrays
	$schema = array(
		"@context" => "http://schema.org",
		"@graph" => array()
	);
	// push configured data to the graph array
	array_push($schema['@graph'], $graphBase);
	// todo format the above data to work with this
    if ($events != "") {
        foreach ($graphEvents as $event) {
            array_push($schema['@graph'], $event);
        }
    };
	array_push($schema['@graph'], $graphBlog);

	//format the data
	$formattedData = json_encode($schema);
  return $formattedData;

}
// add script include to head
function gd_inject_schema() {
        $updatedSchema = gd_write_schema();
        echo '<script type="application/ld+json">'.$updatedSchema.'</script>';
}
add_action('wp_head', __NAMESPACE__ . '\\gd_inject_schema');