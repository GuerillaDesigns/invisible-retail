<?php
use Roots\Sage\Config;
use Roots\Sage\Wrapper;
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?> <?php if (get_field('ic_s_ubi', 'options')){ ?>
          style="background-color:<?php the_field('ic_s_ub_color',
                  'options') ?>;
              <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubiurl', 'options')){?>
                      background-image:url('<?php the_field('ic_s_ubiurl', 'options') ?>');
              <?php } ?>
              <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubit', 'options') ==
                      'repeat-y') {?>
                      background-repeat:<?php the_field('ic_s_ubit', 'options') ?>;
              background-size: 100% auto;
              <?php } ?>
              <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubit', 'options') ==
                      'repeat-x') {?>
                      background-repeat:<?php the_field('ic_s_ubit', 'options') ?>;
              background-size: auto 100%;
              <?php } ?>
              <?php if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubit', 'options') ==
                      'cover'){?>
                      background-size:<?php the_field('ic_s_ubit', 'options') ?>;
              background-repeat:no-repeat;
              <?php } ?>
                  ">
  <?php }?>
  <?php if(get_field('ic_s_obt', 'options')){
    the_field('ic_s_obt', 'options');
  }?>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <div class="ic-wrap">

      <?php
        do_action('get_header');
        get_template_part('templates/header');
      ?>
      <main class="mdl-layout__content" role="document">
        <div class="mdl-grid">
          <?php
          if (Config\display_sidebar()) :
            $col = 'mdl-cell--8-col';
          else :
            $col = 'mdl-cell--12-col';
          endif;
          ?>
          <?php
            $hideMainDisplay = get_field('po_hideMainContent', $pageID);
            if($hideMainDisplay === true ){/*do nothing*/} else { ?>
              <main class="mdl-cell <?php echo $col; ?> <?php $content = get_the_content();
              if(trim($content) == "") { echo 'no_content'; } else{ echo 'has_content';}?>"
                    role="main">
                  <?php include Wrapper\template_path(); ?>
              </main><!-- main -->
          <?php }?>
          <?php get_template_part('templates/ic-modules'); ?>
          <?php

            if (Config\display_sidebar()) : ?>
            <aside class="mdl-cell mdl-cell--4-col" role="complementary">
              <?php include Wrapper\sidebar_path(); ?>
            </aside><!-- sidebar -->
          <?php endif;

          ?>
        </div><!-- /.page-content.mdl-grid -->
      </main><!-- /.mdl-layout__content -->
      <?php
        do_action('get_footer');
        get_template_part('templates/footer');
        wp_footer();
      ?>
    </div><!-- /.mdl-layout -->
  <?php if(get_field('ic_s_cbt', 'options')){
    the_field('ic_s_cbt', 'options');
  }?>
  </body>
</html>
