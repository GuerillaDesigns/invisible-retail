<?php
/**
 * Template Name: Custom Template
 */
  // TODO WRITE INSTRUCTIONS FOR CUSTOMIZATION INCLUTIONS

?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
  <?php get_template_part('templates/ic-modules');?>
<?php endwhile; ?>
