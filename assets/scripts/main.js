/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        // slide down and focus on first field quick form home page
        $( ".mdl-layout__drawer-button" ).click(function() {
          $( ".mdl-layout__drawer" ).slideToggle( "slow", function() {
          });
          if ( $('.mdl-layout__drawer').css('display') === 'flex' ) {
            $('.mdl-layout__drawer').addClass('open');
          }
        });
//----------------------------------------------------------------------------------------
        /* Magnific Poster POP*/
        $('.popup-image').magnificPopup({
          type: 'image'
          // other options
        });

//----------------------------------------------------------------------------------------
        /* Magnific Video POP*/
        $('.popup-video').magnificPopup({
          type:'iframe',
          mainClass: 'mfp-fade',
          iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
            '<h3><div class="mfp-title"></div></h3>'+
            '<div class="mfp-close"></div>'+
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+

            '</div>'
          },
          callbacks: {
            markupParse: function(template, values, item) {
              values.title = item.el.attr('title');
            }
          },
        });

//----------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------
        /* Magnific Video POP*/
        $('.gallery_lightbox').magnificPopup({
            type:'image',
            mainClass: 'mfp-fade',
            markup: '<div class="mfp-figure">'+
            '<div class="mfp-close"></div>'+
            '<div class="mfp-img"></div>'+
            '<div class="mfp-bottom-bar">'+
            '<div class="mfp-title"></div>'+
            '</div>'+
            '</div>', // Popup HTML markup. `.mfp-img` div will be replaced with img tag, `.mfp-close` by close button
            cursor: 'mfp-zoom-out-cur', // Class that adds zoom cursor, will be added to body. Set to null to disable zoom out cursor.
          // titleSrc: 'title', // Attribute of the target element that contains caption for the slide.
            // Or the function that should return the title. For example:
            titleSrc: function(item) {
              return item.el.attr('title');
            },
            gallery:{
              enabled:true,
              navigateByImgClick: false,
              arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
              tPrev: 'Previous (Left arrow key)',
              tNext: 'Next (Right arrow key)'
            }
        });
//----------------------------------------------------------------------------------------
        $('.slides').each(function() {
          $(this).magnificPopup({
            delegate: 'li:not(".clone") a',
            type: 'image',
            gallery: {
              enabled: true,
              navigateByImgClick: true,
              tCounter: '%curr% of %total%', // Markup for "1 of 7" counter
              preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
              titleSrc: function(item) {
                return item.el.find('img').attr('title');
              }
            }
          });
        });

        /* Flexslider */
        $(window).load(function() {
          $('.slider').flexslider({
            slideshow: true,
            namespace: "flex-",
            smoothHeight: true,
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            randomize: true,
            sync: "#carousel",
            useCSS: true,
            touch: true,
            directionNav: true,
            easing: "swing"
          });

        });
        /* END Flexslider */

        /* REMOVE AN IMAGE IF IT MATCHES THE FEATURED IMAGE */
        // get the feature image object - and subsequently it's url
        var $featImg = $(".post_featured_img img");
        var $featImgSrc = $featImg.attr("src");
        //loop through post entry content images to make sure they don't repeat the feature
        // image
        $('.single-post .entry-content img').each(function() {
          if ( $(this).attr('src') === $featImgSrc ) {
            $(this).remove();
          }
        });


      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
