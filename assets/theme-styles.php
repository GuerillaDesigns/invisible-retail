<?php
  $absolute_path = explode('htdocs', $_SERVER['SCRIPT_FILENAME']);
  $wp_load = $absolute_path[0] . 'wp-load.php';
  require_once($wp_load);


  /* Set Colors */
  $color_primary = get_field('ic_ss_bs_pc', 'option');
  $color_secondary = get_field('ic_ss_bs_sc', 'option');
  $color_alternate = get_field('ic_ss_bs_ac', 'option');
  $color_text = get_field('ic_ss_bs_ptc', 'option');
  /* Set Fonts */
  // Main Font
  $font_main = get_field('to_font_main', 'option');
  if ( $font_main == 'open-sans' ) {
    $font_main = "'Open Sans', sans-serif";
  } elseif ( $font_main == 'oswald' ) {
    $font_main = "'Oswald', sans-serif";
  } elseif ( $font_main == 'lora' ) {
    $font_main = "'Lora', serif";
  } else {
    $font_main = "'Roboto', sans-serif";
  }
  // Secondary Font
  $font_secondary = get_field('to_font_secondary', 'option');
  if ( $font_secondary == 'open-sans' ) {
    $font_secondary = "'Open Sans', sans-serif";
  } elseif ( $font_secondary == 'oswald' ) {
    $font_secondary = "'Oswald', sans-serif";
  } else {
    $font_secondary = "'Roboto', sans-serif";
  }
  // Text Font
  $font_text = get_field('to_font_text', 'option');
  if ( $font_text == 'times' ) {
    $font_text = "'Times New Roman', Times, serif";
  } else {
    $font_text = "Arial, Helvetica, sans-serif";
  }

  // Create RGBA for select
  $hex = str_replace("#", "", $color_alternate);
  if(strlen($hex) == 3) {
    $r = hexdec(substr($hex,0,1).substr($hex,0,1));
    $g = hexdec(substr($hex,1,1).substr($hex,1,1));
    $b = hexdec(substr($hex,2,1).substr($hex,2,1));
  } else {
    $r = hexdec(substr($hex,0,2));
    $g = hexdec(substr($hex,2,2));
    $b = hexdec(substr($hex,4,2));
  }
  $select = 'rgba(' . $r . ', ' . $g . ', ' . $b . ', 0.75)';

  // Create RGB
  // Primary
  $rgb_primary = get_field('to_color_primary', 'option');
  $rgb_primary = str_replace("#", "", $rgb_primary);
  if(strlen($rgb_primary) == 3) {
    $r_primary = hexdec(substr($rgb_primary,0,1).substr($rgb_primary,0,1));
    $g_primary = hexdec(substr($rgb_primary,1,1).substr($rgb_primary,1,1));
    $b_primary = hexdec(substr($rgb_primary,2,1).substr($rgb_primary,2,1));
  } else {
    $r_primary = hexdec(substr($rgb_primary,0,2));
    $g_primary = hexdec(substr($rgb_primary,2,2));
    $b_primary = hexdec(substr($rgb_primary,4,2));
  }
  $rgb_primary = $r_primary . ',' . $g_primary . ',' . $b_primary;
  // Secondary
  $rgb_secondary = get_field('to_color_secondary', 'option');
  $rgb_secondary = str_replace("#", "", $rgb_secondary);
  if(strlen($rgb_secondary) == 3) {
    $r_secondary = hexdec(substr($rgb_secondary,0,1).substr($rgb_secondary,0,1));
    $g_secondary = hexdec(substr($rgb_secondary,1,1).substr($rgb_secondary,1,1));
    $b_secondary = hexdec(substr($rgb_secondary,2,1).substr($rgb_secondary,2,1));
  } else {
    $r_secondary = hexdec(substr($rgb_secondary,0,2));
    $g_secondary = hexdec(substr($rgb_secondary,2,2));
    $b_secondary = hexdec(substr($rgb_secondary,4,2));
  }
  $rgb_secondary = $r_secondary . ',' . $g_secondary . ',' . $b_secondary;
  // Alternate
  $rgb_alternate = get_field('to_color_alternate', 'option');
  $rgb_alternate = str_replace("#", "", $rgb_alternate);
  if(strlen($rgb_alternate) == 3) {
    $r_alternate = hexdec(substr($rgb_alternate,0,1).substr($rgb_alternate,0,1));
    $g_alternate = hexdec(substr($rgb_alternate,1,1).substr($rgb_alternate,1,1));
    $b_alternate = hexdec(substr($rgb_alternate,2,1).substr($rgb_alternate,2,1));
  } else {
    $r_alternate = hexdec(substr($rgb_alternate,0,2));
    $g_alternate = hexdec(substr($rgb_alternate,2,2));
    $b_alternate = hexdec(substr($rgb_alternate,4,2));
  }
  $rgb_alternate = $r_alternate . ',' . $g_alternate . ',' . $b_alternate;

  header('Content-type: text/css');
  header('Cache-control: must-revalidate');
?>
::-moz-selection { background-color: <?php echo $select; ?>; color: #fff;}
::selection { background-color: <?php echo $select; ?>; color: #fff;}

/* Theme Set Colors */
.primary-white a, .primary-white .txtHover, .primary-white .colorPrimary<?php echo $child_classes['colorPrimary'] ? ', ' . $child_classes['colorPrimary'] :''; ?> { color: <?php echo $color_primary; ?>; }
.secondary-white a:hover, .secondary-white a:focus, .secondary-white a:active, .secondary-white .txtHover:hover, .secondary-white .txtHover:active, .secondary-white .txtHover:focus, .secondary-white .colorSecondary<?php echo $child_classes['colorSecondary'] ? ', ' . $child_classes['colorSecondary'] :''; ?> { color: <?php echo $color_secondary; ?>; }
.colorAlternate, .alternate-white .colorAlternate<?php echo $child_classes['colorAlternate'] ? ', ' . $child_classes['colorAlternate'] :''; ?> { color: <?php echo $color_alternate; ?>; }

/* Border Color */
.borderPrimary<?php echo $child_classes['borderPrimary'] ? ', ' . $child_classes['borderPrimary'] :''; ?> { border-color: <?php echo $color_primary; ?>; }
.borderSecondary<?php echo $child_classes['borderSecondary'] ? ', ' . $child_classes['borderSecondary'] :''; ?> { border-color: <?php echo $color_secondary; ?>; }
.borderAlternate<?php echo $child_classes['borderAlternate'] ? ', ' . $child_classes['borderAlternate'] :''; ?> { border-color: <?php echo $color_alternate; ?>; }

/* Backgrounds */
button, .btn-content, .gform_button, .bgPrimary, .bgPrimaryAfter:after, .bgPrimaryBefore:before<?php echo $child_classes['bgPrimary'] ? ', ' . $child_classes['bgPrimary'] :''; ?> { background-color: <?php echo $color_primary; ?>; }
button:hover, button:active, button:focus, .btn-content:hover, .btn-content:focus, .btn-content:active, .gform_button:hover, .gform_button:focus, .gform_button:active, .bgSecondary, .bgSecondaryAfter:after, .bgSecondaryBefore:before<?php echo $child_classes['bgSecondary'] ? ', ' . $child_classes['bgSecondary'] :''; ?> { background-color: <?php echo $color_secondary; ?>; }
.bgAlternate, .bgAlternateAfter:after, .bgAlternateBefore:before<?php echo $child_classes['bgAlternate'] ? ', ' . $child_classes['bgAlternate'] :''; ?> { background-color: <?php echo $color_alternate; ?>; }
.bgPrimaryForce { background-color: <?php echo $color_primary; ?> !important; }
.bgSecondaryForce { background-color: <?php echo $color_secondary; ?> !important; }
.bgAlternateForce { background-color: <?php echo $color_alternate; ?> !important; }

/* Gradient Backgrounds */
.gbPrimary<?php echo $child_classes['gbPrimary'] ? ', ' . $child_classes['gbPrimary'] :''; ?> {
background: -moz-linear-gradient(left,  rgba(<?php echo $rgb_primary; ?>,1) 0%, rgba(<?php echo $rgb_primary; ?>,0) 100%);
background: -webkit-linear-gradient(left,  rgba(<?php echo $rgb_primary; ?>,1) 0%,rgba(<?php echo $rgb_primary; ?>,0) 100%);
background: linear-gradient(to right,  rgba(<?php echo $rgb_primary; ?>,1) 0%,rgba(<?php echo $rgb_primary; ?>,0) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $color_primary; ?>', endColorstr='#00<?php echo str_replace("#", "", $color_primary); ?>',GradientType=1 );
}
.gbSecondary<?php echo $child_classes['gbSecondary'] ? ', ' . $child_classes['gbSecondary'] :''; ?> {
background: -moz-linear-gradient(left,  rgba(<?php echo $rgb_secondary; ?>,1) 0%, rgba(<?php echo $rgb_secondary; ?>,0) 100%);
background: -webkit-linear-gradient(left,  rgba(<?php echo $rgb_secondary; ?>,1) 0%,rgba(<?php echo $rgb_secondary; ?>,0) 100%);
background: linear-gradient(to right,  rgba(<?php echo $rgb_secondary; ?>,1) 0%,rgba(<?php echo $rgb_secondary; ?>,0) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $color_secondary; ?>', endColorstr='#00<?php echo str_replace("#", "", $color_secondary); ?>',GradientType=1 );
}
.gbAlternate<?php echo $child_classes['gbAlternate'] ? ', ' . $child_classes['gbAlternate'] :''; ?> {
background: -moz-linear-gradient(left,  rgba(<?php echo $rgb_alternate; ?>,1) 0%, rgba(<?php echo $rgb_alternate; ?>,0) 100%);
background: -webkit-linear-gradient(left,  rgba(<?php echo $rgb_alternate; ?>,1) 0%,rgba(<?php echo $rgb_alternate; ?>,0) 100%);
background: linear-gradient(to right,  rgba(<?php echo $rgb_alternate; ?>,1) 0%,rgba(<?php echo $rgb_alternate; ?>,0) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $color_alternate; ?>', endColorstr='#00<?php echo str_replace("#", "", $color_alternate); ?>',GradientType=1 );
}

/* Fonts */
body, .fontText<?php echo $child_classes['text'] ? ', ' . $child_classes['text'] :''; ?> { font-family: <?php echo $font_text; ?>; }
h1, h2, h3, h4, h5, h6, .fontSecondary<?php echo $child_classes['fontSecondary'] ? ', ' . $child_classes['fontSecondary'] :''; ?> { font-family: <?php echo $font_secondary; ?>; }
h1.headMain, h2.headMain, .fontMain<?php echo $child_classes['fontMain'] ? ', ' . $child_classes['fontMain'] :''; ?> { font-family: <?php echo $font_main; ?>; }
