<?php
	if( have_rows('ic_si_ss', 'options') ):?>
		<div class="s_icons">
		<?php while ( have_rows('ic_si_ss', 'options') ) :
			the_row();
			$socName = get_sub_field('ic_si_ssn', 'options');
			$socIcon = get_sub_field('ic_si_ssicon', 'options');
			$socUrl = get_sub_field('ic_si_ssurl', 'options');
			?>
					<button	class="btn fa <?php echo $socIcon; ?>"
						 	href="<?php echo $socUrl; ?>"
						 	target="_blank"
						><?php echo $socName ;?></button>
			<?php
		endwhile;?>
	</div>
	<?php else :
	endif;
?>