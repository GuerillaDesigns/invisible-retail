<?php use Roots\Sage\Titles;
  $pageID = get_the_ID();
  $titleDisplay = get_field('po_titleDisplayOptions', $pageID);
  $mainDisplay = get_field('po_hideMainContent', $pageID);

  $titleAlign = '';
  if($titleDisplay != 'none'){
    $titleAlign = $titleDisplay;
  }
  $hideTitle = false;
  if($titleDisplay == 'none'){
    $hideTitle = true;
  }
  $content = get_the_content();

?>
<?php if($hideTitle === true || $mainDisplay === true ){ ?>

<?php } else { ?>
  <div class="page-header <?php if(trim($content) == "") { echo 'cm-bkg-trans cm-text-light';}?>" style="<?php if($titleAlign != ''){?>text-align:<?php echo $titleAlign;?>;<?php } ?>">
    <h1><?php echo Titles\title();?></h1>
  </div>
<?php } ?>
