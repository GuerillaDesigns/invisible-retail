<?php /* #zombiecode
    if ( is_active_sidebar( 'sidebar-mega-footer' ) ) { ?>
  <footer class="mdl-mega-footer" role="contentinfo">
    <div class="mdl-mega-footer__middle-section  mdl-grid">
      <?php dynamic_sidebar('sidebar-mega-footer'); ?>
    </div>
  </footer>
<?php } */ ?>
<footer class="mdl-mini-footer" role="contentinfo">
  <?php if ( is_active_sidebar( 'sidebar-mini-footer' ) ) { ?>
    <?php dynamic_sidebar('sidebar-mini-footer'); ?>
  <?php } ?>
  <?php get_template_part('templates/phone-dir'); ?>
  <?php get_template_part('templates/address'); ?>
  <?php get_template_part('templates/social'); ?>
</footer>




