<form role="search" method="get" class="search-form form-inline" action="<?php echo esc_url(home_url('/')); ?>">
  <div class="input-group">
    <input type="search" value="<?php echo get_search_query(); ?>" name="s" class="search-field form-control" placeholder="<?php _e('Press enter to search'); ?>" required>
   
  </div>
</form>
<span class="input-group-btn">
    <button type="submit" class="search-submit toggler btn btn-default"><i class="material-icons">search</i></button>
</span>