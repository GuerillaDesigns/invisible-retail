<?php

  $monday = get_field('ic_ss_bi_bh_mon', 'options');
  $monHours = $monday['hd_hours'];
  $monSftCnt = "0";
  foreach ($monHours as $shift ){
    $monSftCnt++;
  }
  $tuesday = get_field('ic_ss_bi_bh_tues', 'options');
  $tuesHours = $tuesday['hd_hours'];
  $tuesSftCnt = "0";
  foreach ($tuesHours as $shift ){
    $tuesSftCnt++;
  }
  $wednesday = get_field('ic_ss_bi_bh_wed', 'options');
  $wedHours = $wednesday['hd_hours'];
  $wedSftCnt = "0";
  foreach ($wedHours as $shift ){
    $wedSftCnt++;
  }
  $thursday = get_field('ic_ss_bi_bh_thur', 'options');
  $thurHours = $thursday['hd_hours'];
  $thurSftCnt = "0";
  foreach ($thurHours as $shift ){
    $thurSftCnt++;
  }
  $friday = get_field('ic_ss_bi_bh_fri', 'options');
  $friHours = $friday['hd_hours'];
  $friSftCnt = "0";
  foreach ($friHours as $shift ){
    $friSftCnt++;
  }
  $saturday = get_field('ic_ss_bi_bh_sat', 'options');
  $satHours = $saturday['hd_hours'];
  $satSftCnt = "0";
  foreach ($satHours as $shift ){
    $satSftCnt++;
  }
  $sunday = get_field('ic_ss_bi_bh_sun', 'options');
  $sunHours = $sunday['hd_hours'];
  $sunSftCnt = "0";
  foreach ($sunHours as $shift ){
    $sunSftCnt++;
  }

  $bOpen = false;
  // MONDAY
  if (current_time('w') == 1) {

    if( $monday['hd_open'] == 1 ){

       if( $monSftCnt == 1) {
         $monSft1 = [];
         $monSft1 = $monHours[0];
         $monSft1Open = $monSft1['hd_opening_time'];
         $monSft1Close = $monSft1['hd_closing_time'];
         if(current_time('H:i:s') >= $monSft1Open && current_time('H:i:s') < $monSft1Close) {
           // the thing you want on monday 0:00 (12 am) to 17:00 (5pm)
           $bOpen = true;
         }
       }elseif($monSftCnt == 2 ){
         $monSft1 = [];
         $monSft1 = $monHours[0];
         $monSft1Open = $monSft1['hd_opening_time'];
         $monSft1Close = $monSft1['hd_closing_time'];
         $monSft2 = [];
         $monSft2 = $monHours[1];
         $monSft2Open = $monSft2['hd_opening_time'];
         $monSft2Close = $monSft2['hd_closing_time'];
        if (current_time('H:i:s') >= $monSft1Open && current_time('H:i:s') < $monSft1Close || current_time('H:i:s') >= $monSft2Open
                && current_time('H:i:s') < $monSft2Close) {
          $bOpen = true;
        }
      }elseif( $monSftCnt == 3 ){
         $monSft1 = [];
         $monSft1 = $monHours[0];
         $monSft1Open = $monSft1['hd_opening_time'];
         $monSft1Close = $monSft1['hd_closing_time'];
         $monSft2 = [];
         $monSft2 = $monHours[1];
         $monSft2Open = $monSft2['hd_opening_time'];
         $monSft2Close = $monSft2['hd_closing_time'];
         $monSft3 = [];
         $monSft3 = $monHours[2];
         $monSft3Open = $monSft3['hd_opening_time'];
         $monSft3Close = $monSft3['hd_closing_time'];
         if (current_time('H:i:s') >= $monSft1Open && current_time('H:i:s') < $monSft1Close || current_time('H:i:s') >= $monSft2Open
                 && current_time('H:i:s') < $monSft2Close || current_time('H:i:s') >= $monSft3Open
                 && current_time('H:i:s') < $monSft3Close) {
           $bOpen = true;
         }
      }
    }
  }elseif (current_time('w') == 2) { // Tuesday
    if( $tuesday['hd_open'] == 1 ){
      if( $tuesSftCnt == 1) {
        $tuesSft1 = [];
        $tuesSft1 = $tuesHours[0];
        $tuesSft1Open = $tuesSft1['hd_opening_time'];
        $tuesSft1Close = $tuesSft1['hd_closing_time'];
        if(current_time('H:i:s') >= $tuesSft1Open && current_time('H:i:s') < $tuesSft1Close) {
          // the thing you want on monday 0:00 (12 am) to 17:00 (5pm)
          $bOpen = true;
        }
      }elseif($tuesSftCnt == 2 ){
        $tuesSft1 = [];
        $tuesSft1 = $tuesHours[0];
        $tuesSft2 = [];
        $tuesSft2 = $tuesHours[1];
        $tuesSft1Open = $tuesSft1['hd_opening_time'];
        $tuesSft1Close = $tuesSft1['hd_closing_time'];
        $tuesSft2Open = $tuesSft2['hd_opening_time'];
        $tuesSft2Close = $tuesSft2['hd_closing_time'];
        if (current_time('H:i:s') >= $tuesSft1Open && current_time('H:i:s') < $tuesSft1Close || current_time('H:i:s') >= $tuesSft2Open
                && current_time('H:i:s') < $tuesSft2Close) {
          $bOpen = true;
        }
      }elseif( $tuesSftCnt == 3 ){
        $tuesSft1 = [];
        $tuesSft1 = $tuesHours[0];
        $tuesSft1Open = $tuesSft1['hd_opening_time'];
        $tuesSft1Close = $tuesSft1['hd_closing_time'];
        $tuesSft2 = [];
        $tuesSft2 = $tuesHours[1];
        $tuesSft2Open = $tuesSft2['hd_opening_time'];
        $tuesSft2Close = $tuesSft2['hd_closing_time'];
        $tuesSft3 = [];
        $tuesSft3 = $tuesHours[2];
        $tuesSft3Open = $tuesSft3['hd_opening_time'];
        $tuesSft3Close = $tuesSft3['hd_closing_time'];
        if (current_time('H:i:s') >= $tuesSft1Open && current_time('H:i:s') < $tuesSft1Close || current_time('H:i:s') >= $tuesSft2Open
                && current_time('H:i:s') < $tuesSft2Close || current_time('H:i:s') >= $tuesSft3Open
                && current_time('H:i:s') < $tuesSft3Close) {
          $bOpen = true;
        }
      }
    }
  }elseif (current_time('w') == 3) { // Wednesday
    if( $wednesday['hd_open'] == 1 ){
      if( $wedSftCnt == 1) {
        $wedSft1 = [];
        $wedSft1 = $wedHours[0];
        $wedSft1Open = $wedSft1['hd_opening_time'];
        $wedSft1Close = $wedSft1['hd_closing_time'];
        if(current_time('H:i:s') >= $wedSft1Open && current_time('H:i:s') < $wedSft1Close) {
          // the thing you want on monday 0:00 (12 am) to 17:00 (5pm)
          $bOpen = true;
        }
      }elseif($wedSftCnt == 2 ){
        $wedSft1 = [];
        $wedSft1 = $wedHours[0];
        $wedSft1Open = $wedSft1['hd_opening_time'];
        $wedSft1Close = $wedSft1['hd_closing_time'];
        $wedSft2 = [];
        $wedSft2 = $wedHours[1];
        $wedSft2Open = $wedSft2['hd_opening_time'];
        $wedSft2Close = $wedSft2['hd_closing_time'];
        if (current_time('H:i:s') >= $wedSft1Open && current_time('H:i:s') < $wedSft1Close || current_time('H:i:s') >= $wedSft2Open
                && current_time('H:i:s') < $wedSft2Close) {
          $bOpen = true;
        }
      }elseif( $wedSftCnt == 3 ){
        $wedSft1 = [];
        $wedSft1 = $wedHours[0];
        $wedSft1Open = $wedSft1['hd_opening_time'];
        $wedSft1Close = $wedSft1['hd_closing_time'];
        $wedSft2 = [];
        $wedSft2 = $wedHours[1];
        $wedSft2Open = $wedSft2['hd_opening_time'];
        $wedSft2Close = $wedSft2['hd_closing_time'];
        $wedSft3 = [];
        $wedSft3 = $wedHours[2];
        $wedSft3Open = $wedSft3['hd_opening_time'];
        $wedSft3Close = $wedSft3['hd_closing_time'];
        if (current_time('H:i:s') >= $wedSft1Open && current_time('H:i:s') < $wedSft1Close || current_time('H:i:s') >= $wedSft2Open
                && current_time('H:i:s') < $wedSft2Close || current_time('H:i:s') >= $wedSft3Open
                && current_time('H:i:s') < $wedSft3Close) {
          $bOpen = true;
        }
      }
    }
  }elseif (current_time('w') == 4) { // THURSDAY
    if( $thursday['hd_open'] == 1 ){
      if( $thurSftCnt == 1) {
        $thurSft1 = [];
        $thurSft1 = $thurHours[0];
        $thurSft1Open = $thurSft1['hd_opening_time'];
        $thurSft1Close = $thurSft1['hd_closing_time'];
        if(current_time('H:i:s') >= $thurSft1Open && current_time('H:i:s') < $thurSft1Close) {
          // the thing you want on monday 0:00 (12 am) to 17:00 (5pm)
          $bOpen = true;
        }
      }elseif($thurSftCnt == 2 ){
        $thurSft1 = [];
        $thurSft1 = $thurHours[0];
        $thurSft2 = [];
        $thurSft2 = $thurHours[1];
        $thurSft1Open = $thurSft1['hd_opening_time'];
        $thurSft1Close = $thurSft1['hd_closing_time'];
        $thurSft2Open = $thurHours['hd_opening_time'];
        $thurSft2Close = $thurSft2['hd_closing_time'];
        if (current_time('H:i:s') >= $thurSft1Open && current_time('H:i:s') < $thurSft1Close || current_time('H:i:s') >= $thurSft2Open
                && current_time('H:i:s') < $thurSft2Close) {
          $bOpen = true;
        }
      }elseif( $thurSftCnt == 3 ){
        $thurSft1 = [];
        $thurSft1 = $thurHours[0];
        $thurSft2 = [];
        $thurSft2 = $thurHours[1];
        $thurSft3 = [];
        $thurSft3 = $thurHours[2];
        $thurSft1Open = $thurSft1['hd_opening_time'];
        $thurSft1Close = $thurSft1['hd_closing_time'];
        $thurSft2Open = $thurSft2['hd_opening_time'];
        $thurSft2Close = $thurSft2['hd_closing_time'];
        $thurSft3Open = $thurSft3['hd_opening_time'];
        $thurSft3Close = $thurSft3['hd_closing_time'];
        if (current_time('H:i:s') >= $thurSft1Open && current_time('H:i:s') < $thurSft1Close || current_time('H:i:s') >= $thurSft2Open
                && current_time('H:i:s') < $thurSft2Close || current_time('H:i:s') >= $thurSft3Open
                && current_time('H:i:s') < $thurSft3Close) {
          $bOpen = true;
        }
      }
    }
  }elseif (current_time('w') == 5) { // FRIDAY
    if( $friday['hd_open'] == 1 ){
      if( $friSftCnt == 1) {
        $friSft1 = [];
        $friSft1 = $friHours[0];
        $friSft1Open = $friSft1['hd_opening_time'];
        $friSft1Close = $friSft1['hd_closing_time'];
        if(current_time('H:i:s') >= $friSft1Open && current_time('H:i:s') < $friSft1Close) {
          // the thing you want on monday 0:00 (12 am) to 17:00 (5pm)
          $bOpen = true;
        }
      }elseif($monSftCnt == 2 ){
        $friSft1 = [];
        $friSft1 = $friHours[0];
        $friSft2 = [];
        $friSft2 = $friHours[1];
        $friSft1Open = $friSft1['hd_opening_time'];
        $friSft1Close = $friSft1['hd_closing_time'];
        $friSft2Open = $friSft2['hd_opening_time'];
        $friSft2Close = $friSft2['hd_closing_time'];
        if (current_time('H:i:s') >= $friSft1Open && current_time('H:i:s') < $friSft1Close || current_time('H:i:s') >= $friSft2Open
                && current_time('H:i:s') < $friSft2Close) {
          $bOpen = true;
        }
      }elseif( $monSftCnt == 3 ){
        $friSft1 = [];
        $friSft1 = $friHours[0];
        $friSft2 = [];
        $friSft2 = $friHours[1];
        $friSft3 = [];
        $friSft3 = $friHours[2];
        $friSft1Open = $friSft1['hd_opening_time'];
        $friSft1Close = $friSft1['hd_closing_time'];
        $friSft2Open = $friSft2['hd_opening_time'];
        $friSft2Close = $friSft2['hd_closing_time'];
        $friSft3Open = $friSft3['hd_opening_time'];
        $friSft3Close = $friSft3['hd_closing_time'];
        if (current_time('H:i:s') >= $friSft1Open && current_time('H:i:s') < $friSft1Close || current_time('H:i:s') >= $friSft2Open
                && current_time('H:i:s') < $friSft2Close || current_time('H:i:s') >= $friSft3Open
                && current_time('H:i:s') < $friSft3Close) {
          $bOpen = true;
        }
      }
    }
  }elseif (current_time('w') == 6) { // SATURDAY

    if( $saturday['hd_open'] == 1 ){
      if( $satSftCnt == 1) {
        $satSft1 = [];
        $satSft1 = $satHours[0];
        $satSft1Open = $satSft1['hd_opening_time'];
        $satSft1Close = $satSft1['hd_closing_time'];
        if(current_time('H:i:s') >= $satSft1Open && current_time('H:i:s') < $satSft1Close) {
          // the thing you want on monday 0:00 (12 am) to 17:00 (5pm)
          $bOpen = true;
        }
      }elseif($monSftCnt == 2 ){
        $satSft1 = [];
        $satSft1 = $satHours[0];
        $satSft2 = [];
        $satSft2 = $satHours[1];
        $satSft1Open = $satSft1['hd_opening_time'];
        $satSft1Close = $satSft1['hd_closing_time'];
        $satSft2Open = $satSft2['hd_opening_time'];
        $satSft2Close = $satSft2['hd_closing_time'];
        if (current_time('H:i:s') >= $satSft1Open && current_time('H:i:s') < $satSft1Close || current_time('H:i:s') >= $satSft2Open
                && current_time('H:i:s') < $satSft2Close) {
          $bOpen = true;
        }
      }elseif( $monSftCnt === 3 ){
        $satSft1 = [];
        $satSft1 = $satHours[0];
        $satSft2 = [];
        $satSft2 = $satHours[1];
        $satSft3 = [];
        $satSft3 = $satHours[2];
        $satSft1Open = $satSft1['hd_opening_time'];
        $satSft1Close = $satSft1['hd_closing_time'];
        $satSft2Open = $satSft2['hd_opening_time'];
        $satSft2Close = $satSft2['hd_closing_time'];
        $satSft3Open = $satSft3['hd_opening_time'];
        $satSft3Close = $satSft3['hd_closing_time'];
        if (current_time('H:i:s') >= $satSft1Open && current_time('H:i:s') < $satSft1Close || current_time('H:i:s') >= $satSft2Open
                && current_time('H:i:s') < $satSft2Close || current_time('H:i:s') >= $satSft3Open
                && current_time('w') < $satSft3Close) {
          $bOpen = true;
        }
      }
    }
  }elseif (current_time('w') == 0) { // SUNDAY
    if( $sunday['hd_open'] == 1 ){
      if( $sunSftCnt == 1) {
        $sunSft1 = [];
        $sunSft1 = $sunHours[0];
        $sunSft1Open = $sunSft1['hd_opening_time'];
        $sunSft1Close = $sunSft1['hd_closing_time'];
        if(current_time('H:i:s') >= $sunSft1Open && current_time('H:i:s') < $sunSft1Close) {
          // the thing you want on monday 0:00 (12 am) to 17:00 (5pm)
          $bOpen = true;
        }
      }elseif($monSftCnt === 2 ){
        $sunSft1 = [];
        $sunSft1 = $sunHours[0];
        $sunSft2 = [];
        $sunSft2 = $sunHours[1];
        $sunSft1Open = $sunSft1['hd_opening_time'];
        $sunSft1Close = $sunSft1['hd_closing_time'];
        $sunSft2Open = $sunSft2['hd_opening_time'];
        $sunSft2Close = $sunSft2['hd_closing_time'];
        if (current_time('H:i:s') >= $sunSft1Open && current_time('H:i:s') < $sunSft1Close || current_time('H:i:s') >= $sunSft2Open
                && current_time('H:i:s') < $sunSft2Close) {
          $bOpen = true;
        }
      }elseif( $monSftCnt === 3 ){
        $sunSft1 = [];
        $sunSft1 = $sunHours[0];
        $sunSft2 = [];
        $sunSft2 = $sunHours[1];
        $sunSft3 = [];
        $sunSft3 = $sunHours[2];
        $sunSft1Open = $sunSft1['hd_opening_time'];
        $sunSft1Close = $sunSft1['hd_closing_time'];
        $sunSft2Open = $sunSft2['hd_opening_time'];
        $sunSft2Close = $sunSft2['hd_closing_time'];
        $sunSft3Open = $sunSft3['hd_opening_time'];
        $sunSft3Close = $sunSft3['hd_closing_time'];
        if (current_time('H:i:s') >= $sunSft1Open && current_time('H:i:s') < $sunSft1Close || current_time('H:i:s') >= $sunSft2Open
                && current_time('H:i:s') < $sunSft2Close || current_time('H:i:s') >= $sunSft3Open
                && current_time('H:i:s') < $sunSft3Close) {
          $bOpen = true;
        }
      }
    }
  }


$test = $satSft1;
if($bOpen === true){
  $testVar = 'bOpen === true';
  $openText = get_field('ic_ss_bi_os_opentext', 'options');
  $openClass = 'bh-open';
  $iconClass = 'fa fa-lightbulb-o';
}
if($bOpen === false){
  $testVar = 'bOpen === false';
  $openText = get_field('ic_ss_bi_os_closedtext', 'options');
  $openClass = 'bh-closed';
  $iconClass = 'fa fa-moon-o';
}
?>
<div class="open-light <?php echo $iconClass; ?> <?php echo $openClass; ?>">
  <?php the_sub_field('ic_ss_bi_bh_d', 'option'); ?>
  <span class="open-text"><?php echo $openText;?></span>
</div>

