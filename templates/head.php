<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php the_field('ic_ss_bs_fi', 'option'); ?>" type="image/x-icon"/>
  <?php if(get_field('ic_s_hs', 'options')){
    the_field('ic_s_hs', 'options');
  }?>
  
  <?php  
  $heroBannerUrl = get_field('ic_s_hb', 'options' )['url'];
  if($heroBannerUrl != ""){ ?>
    <link rel="preload" as="image" href="<?php echo $heroBannerUrl;?>">
  <?php } ?>
  <?php 
  if(get_field('ic_s_ubi', 'options') && get_field('ic_s_ubiurl', 'options')){?>
    <link rel="preload" as="image" href="<?php the_field('ic_s_ubiurl', 'options') ?>">
  <?php } ?>
  <?php
  $primaryLogo = "";
  $primaryLogo = get_field('ic_ss_bs_pl', 'options');
  $primaryLogoUrl = $primaryLogo['url'];
  if($primaryLogoUrl != ''){ ?>
    <link rel="preload" as="image" href="<?php echo $primaryLogoUrl;?>">
  <?php } ?>
  
  <?php wp_head(); ?>
</head>