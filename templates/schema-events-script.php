<script type="application/ld+json">
{
  	"@context": "http://schema.org",

<?php //-- Schema Events --
    // Retrieve the next 5 upcoming events
    $events = tribe_get_events( array(
        'posts_per_page' => 5,
        'start_date' => strtotime(now)
    ) );
    if ( $events != "") :   ?>
        "@type": "Event",
        <?php
        // Loop through the events: set up each one as
        // the current post then use template tags to
        // display the title and content
        foreach ( $events as  $post) {
            $event_id = $post->ID;


            setup_postdata( $post );?>
            "name": "<?php echo get_the_title( $event_id ); ?>",
            "location": {
            "@type": "Place",
            "address": {
            "@type": "PostalAddress",
            "streetAddress" : "<?php echo tribe_get_address($event_id); ?>"
            "addressLocality": "<?php echo tribe_get_city ($event_id); ?>",
            "addressRegion": "<?php echo tribe_get_stateprovince($event_id); ?>",
            "postalCode": "<?php echo tribe_get_zip($event_id); ?>"
            },
            },
            "startDate": "<?php echo tribe_get_start_date($event_id); ?>",
            "url": "<?php echo tribe_get_event_link($event_id); ?>"
        <?php } ?>
    <?php endif; ?>
}
</script>