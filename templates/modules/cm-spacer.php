<?php if( get_row_layout() == 'cm_spacer' ) : ?>
  <div class="ic-module mdl-cell mdl-cell--12-col cm-spacer <?php if(get_sub_field('cm_spacer_bkg') === true){echo 'cm-bkg-trans';} ?>" style="height: <?php the_sub_field('cm_spacer_height');?>px"></div>
<?php endif;