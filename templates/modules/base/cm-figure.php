<?php
  // set up popup content url
  $popupUrl = "";
  if($layoutType == "image"){
    $popupUrl = $imgUrl;
  }elseif($layoutType == "video"){
    $popupUrl = $videoTypeUrl;
  }
  // set up popup class
  $popupClass = "";
  if ( $layoutType == "image" && $lightbox === true ){
    $popupClass = "popup-image";
  } elseif ( $layoutType == "video" ){
    $popupClass = "popup-video";
  }
  // set up popup icon
  $popupIcon = "";
  if ( $layoutType == "image" && $lightbox === true ){
    $popupIcon = "fa fa-expand";
  } elseif ( $layoutType == "video" ){
    $popupIcon = "fa fa-play-circle-o ";
  }
?>
<figure class="cm-figure" style="background-image:url('<?php echo $figureUrl; ?>');">
  <?php if( $layoutType == 'video' && $lightbox === false ){ ?>
    <iframe
            width="560"
            height="315"
            src="<?php echo $popupUrl; ?>"
            frameborder="0"
            alt="<?php  echo $imgAlt; ?>"
            allowfullscreen
    ></iframe>
  <?php } ?>
  <?php if( $layoutType != 'text' && $lightbox === true ){ ?>
    <a href="<?php echo $popupUrl; ?>" class="figure-link <?php echo $popupClass; ?> <?php echo $popupIcon; ?>" alt="<?php echo $imgAlt; ?>"></a>
  <?php } ?>
</figure>
<?php if( $text != ''){ ?>
<figcaption class="cm-figcaption">
    <div class="cm-text">
      <?php echo $text; ?>
    </div>
</figcaption>
<?php } ?>