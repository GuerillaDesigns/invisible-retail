<?php if( get_row_layout() == 'cm_single_col' ) :
  $layout = 'single';
  $layoutColumns = '12';
  $cm_col_count = 1;
  $module = get_sub_field('cm_single');
  $layoutType = $module['type'];
  $transBkg = $module['bkg'];
  ?>
  <div class="ic-module-row mdl-grid">
    <section class="ic-module mdl-cell cm-<?php echo $layoutType;?> <?php echo $layout; ?>-<?php echo $layoutType; ?>-<?php echo $cm_col_count; ?> mdl-cell--<?php echo $layoutColumns; ?>-col cm-<?php echo $layout; ?> cm-<?php echo $layout;?>-<?php echo $layoutType; ?> <?php if( $transBkg === true ){ echo "cm-bkg-trans"; } ?> <?php if( $transBkg === true ){ echo $txt_color; } ?>">
      <?php include('base/cm-base.php'); ?>
    </section>
  </div>
  <?php
endif;