<?php
if( get_row_layout() == 'cm_double_col' ) :
  $layoutColumn = get_sub_field('cm_double_layout');
  ?>
  <div class="ic-module-row mdl-grid">
  <?php
  if( have_rows('cm_double_cols') ):
    $cm_col_count = 0;
    while( have_rows('cm_double_cols') ):
      the_row();
      $cm_col_count++;
      $layout = 'double';
      $layoutColumns = '';
      if($layoutColumn == 'half'){
        $layoutColumns = '6';
      }elseif($layoutColumn == 'bigLeft'){
        if($cm_col_count === 1){
          $layoutColumns = '8';
        }
        if($cm_col_count === 2){
          $layoutColumns = '4';
        }
      }elseif($layoutColumn == 'bigRight'){
        if($cm_col_count === 1){
          $layoutColumns = '4';
        }
        if($cm_col_count === 2){
          $layoutColumns = '8';
        }
      }
      $module = get_sub_field('cm_double');
      $layoutType = $module['type'];
      $transBkg = $module['bkg'];
      ?>
      <section class="ic-module mdl-cell cm-<?php echo $layoutType; ?> <?php echo $layout; ?>-<?php echo $layoutType; ?>-<?php echo $cm_col_count; ?> mdl-cell--<?php echo $layoutColumns; ?>-col cm-<?php echo $layout; ?> cm-<?php echo $layout; ?>-<?php echo $layoutType; ?> <?php if( $transBkg === true ){ echo "cm-bkg-trans"; } ?> <?php if( $transBkg === true ){ echo $txt_color; } ?>">
        <?php include('base/cm-base.php'); ?>
      </section>
      <?php
    endwhile;
  endif; ?>
  </div>
<?php
endif;



