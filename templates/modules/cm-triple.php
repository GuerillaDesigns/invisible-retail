<?php
if( get_row_layout() == 'cm_triple_col' ) : ?>
    <div class="ic-module-row mdl-grid">
  <?php
  if( have_rows('cm_triple_cols') ):
  $cm_col_count = 0;
  while( have_rows('cm_triple_cols') ):
  the_row();
    $cm_col_count++;
    $layout = 'triple';
    $layoutColumns = '4';
    $module = get_sub_field('cm_triple');
    $layoutType = $module['type'];
    $transBkg = $module['bkg'];
    ?>
    <section class="ic-module mdl-cell cm-<?php echo $layoutType; ?> <?php echo $layout; ?>-<?php echo $layoutType; ?>-<?php echo $cm_col_count; ?> mdl-cell--<?php echo $layoutColumns; ?>-col cm-<?php echo $layout; ?> cm-<?php echo $layout;
    ?>-<?php echo $layoutType; ?> <?php if( $transBkg === true ){ echo "cm-bkg-trans"; } ?> <?php if( $transBkg === true ){ echo $txt_color; } ?>">
      <?php include('base/cm-base.php'); ?>
    </section>
    <?php
  endwhile;
  endif; ?>
      </div>
<?php
endif;

