<?php if( get_row_layout() == 'cm_posts_grid' ) : ?>
  <div class="ic-module-row mdl-grid">
      <?php
        $layoutColumns = get_sub_field('cm_posts_cols');
        $transBkg = get_sub_field('cm_posts_bkg');
        $linkType = get_sub_field('cm_posts_link_type');
        $terms = get_sub_field('cm_posts_cat');
        $catList = implode(',',$terms);
        $catListClass = implode('-',$terms);
        $postLimit = get_sub_field('cm_posts_limit');
        $showText = get_sub_field('cm_posts_title_and_excerpt');
        $centerText = get_sub_field('cm_align_center');
        $postOrder = get_sub_field('cm_posts_order');

        $cm_query_args = "";
        $cm_query_args = array(
                'post_type' => 'post',
                'cat' => $catList,
                'post_status' => 'publish',
                'posts_per_page' => $postLimit,
                'orderby' => 'date',
                'order' => $postOrder,
        );
        $temp = $cm_cat_query;
        $cm_cat_query = null;
        $cm_cat_query = new WP_Query( $cm_query_args );
        ?>
      <?php if( $cm_cat_query ): ?>
      <?php
        // The Loop
        if ( $cm_cat_query->have_posts() ) :
        while ( $cm_cat_query->have_posts() ) :
        $cm_cat_query->the_post();
          setup_postdata( $post );
          //Build post thumbnail url's
          $post_thumbnail_id = get_post_thumbnail_id($post->ID);
          $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
          $postTitle = get_the_title($post->ID);
          $postTitleClean = $postTitle;
          $videoUrl = get_field('ic_post_video_url');

          $cm_postImgUrl = '';
          if($post_thumbnail_url != ''){
            $cm_postImgUrl = $post_thumbnail_url;
          } elseif ($heroBannerUrl != ''){
            $cm_postImgUrl = $heroBannerUrl;
          } else {
            $cm_postImgUrl = $primaryLogoUrl;
          }
          $popupIcon = '';
          if($videoUrl != '' && $linkType == 'gallery_lightbox'){
            $popupIcon = 'fa fa-play-circle ';
          }elseif($linkType == 'gallery_lightbox'){
            $popupIcon = 'fa fa-expand ';
          }elseif($linkType == '_self'){
            $popupIcon = 'fa fa-chevron-right ';
          }
          // Do Stuff ?>
          <section class="ic-module cm-posts cm-posts-cat-<?php echo $catListClass; ?> mdl-cell mdl-cell--<?php echo $layoutColumns; ?>-col <?php if( $transBkg === true ){ echo "cm-bkg-trans"; } ?> <?php if( $transBkg === true ){ echo $txt_color; } ?>">
            <figure
              class="cm-figure"
              style="background-image:url('<?php echo $cm_postImgUrl; ?>');"
            >
              <a href="<?php
                  if($videoUrl != ''){
                    echo $videoUrl;
                  }elseif($linkType == 'gallery_lightbox'){
                    echo $cm_postImgUrl;
                  } elseif($linkType == '_self'){
                    the_permalink();
                  } ?>"
                class="cm-posts-link figure-link <?php echo $popupIcon; ?>  <?php
                  if($videoUrl != '' && $linkType == 'gallery_lightbox'){
                    echo 'popup-video';
                  }elseif($linkType == 'gallery_lightbox'){
                    echo $linkType;
                  } ?>"
                 title="<?php echo $postTitleClean; ?>"
                <?php if($linkType == '_self'){?>
                target="_self"

                <?php } ?>
              ></a>
          </figure>
        <?php if($showText === true){?>
          <figcaption class="cm-figcaption">
            <div class="cm-text <?php if($centerText === true){echo 'center-text';}?>">
              <h4><a href="<?php the_permalink();?>"><?php echo $postTitleClean; ?></a></h4>
              <p><?php the_excerpt(); ?></p>
            </div>
          </figcaption>
        <?php } ?>
          </section>
      <?php  endwhile;
          $temp = $cm_cat_query;
          $cm_cat_query = null;
           wp_reset_postdata();
        endif;
      ?>
  <?php endif; ?>
  </div>
<?php endif;