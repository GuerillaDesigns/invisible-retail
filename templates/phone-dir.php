<div id="phonedir">
  <ul class="phoneNumbers">
    <?php
      $mainPhone = get_field('ic_ss_bi_bmp', 'options');
      $DialmainPhone = preg_replace('/[^0-9]/', '', $mainPhone);
    ?>
    <li class="phone">
      <a href="tel:<?php echo $DialmainPhone; ?>"><span class="ptitle">Main Phone :</span> <span class="pnumber"><?php echo $mainPhone;?></span></a>
    </li>
    <?php if( have_rows('ic_ss_bi_bps', 'options') ):
      $pI = "0";
      while( have_rows('ic_ss_bi_bps', 'options') ): the_row();
        // vars
        $pI++;
        $pLable = get_sub_field('ic_ss_bi_bp_nl', 'options');
        $pNumber = get_sub_field('ic_ss_bi_bp_n', 'options');
        $DialpNumber = preg_replace('/[^0-9]/', '', $pNumber);
        ?>
        <li class="phone<?php echo $pI; ?>">
          <a href="tel:<?php echo $DialpNumber; ?>"><span class="ptitle"><?php echo $pLable; ?></span><span class="pnumber"><?php echo $pNumber; ?></span></a>
        </li>
      <?php endwhile; ?>
    <?php endif; ?>
  </ul>
</div>