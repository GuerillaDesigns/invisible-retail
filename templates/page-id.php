<?php

  /*TODO
      SET POST ID VARIABLE
      REFINE THIS TO HANDLE SINGLE POST IDS
      MAKE SURE IT HANDLES ALL VITUAL PAGES CORRECTLY
      AND IN THE FUTURE ANY CUSTOM POST TYPES

      TRY TO FIGURE OUT THIS FUNCTION get_the_post_id() - in the extras file
  */
  $page_id = get_the_ID();

  if(is_home() || is_single()) :
    $page_id = get_option('page_for_posts');
  endif;
