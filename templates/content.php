<?php
  setup_postdata( $post );
  //Build post thumbnail url's
  $post_thumbnail_id = get_post_thumbnail_id($post->ID);
  $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
?>
<article <?php post_class(); ?>>
  <?php if ($post_thumbnail_url != ""){?>
    <figure class="post_featured_img">
      <img src="<?php echo $post_thumbnail_url;?>"/>
      <a href="<?php echo $post_thumbnail_url;?>" class="fa fa-expand popup-image cm-posts-link post-figure-expand"></a>
    </figure>
  <?php }?>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php // get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
